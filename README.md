# Tests OS



## Code du projet
Ce dépôt est destiné à être utilisé dans le cadre du cours Applications informatiques:
Projet 15: Aider les étudiant-e-s à mieux comprendre les attentes d'un TP

## Utilisation
Tests Python pour les TDs de Systèmes d'exploitation:
Les tests automatisés, pour les travaux pratiques TP2 sur le hachage et TP5 sur le client-serveur, permettront de vérifier si les programmes créer fonctionnent conformément aux spécifications demandèes, en testant ces différentes fonctionnalités.

## Explication de l'usage
Hash:

./path_to/test.py /path_to/program -optional_arguments

Client-Server example:

./test.py ./TP-Correct/server 50000 [1,-1,0,2] 3
./test.py ./TP-Correct/client 50000 [1,-1,0,2] 3  -c



## Project status
Fini.
